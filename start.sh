#!/bin/bash

APPD_ROOT="/opt/appdynamics"
APPD_EUM="$APPD_ROOT/eum"
APPD_EUM_DB="$APPD_EUM/mysql"
APPD_EUM_PROC="$APPD_EUM/eum-processor"
APPD_EUM_PROC_PROP="$APPD_EUM_PROC/bin/eum.properties"

if [ -n "${APPDYNAMICS_EUM_ANALYTICS_SCHEME:+1}" ] \
    && [ -n "${APPDYNAMICS_EUM_ANALYTICS_HOST:+1}" ] \
    && [ -n "${APPDYNAMICS_EUM_ANALYTICS_PORT:+1}" ] \
    && [ -n "${APPDYNAMICS_EUM_ANALYTICS_KEY:+1}" ]
then
    sed -i -e "/analytics.enabled/c\analytics\.enabled=true" \
        $APPD_EUM_PROC_PROP
    sed -i -e "/analytics.serverScheme/c\analytics\.serverScheme=$APPDYNAMICS_EUM_ANALYTICS_SCHEME" \
        $APPD_EUM_PROC_PROP
    sed -i -e "/analytics.serverHost/c\analytics\.serverHost=$APPDYNAMICS_EUM_ANALYTICS_HOST" \
        $APPD_EUM_PROC_PROP
    sed -i -e "/analytics.port/c\analytics\.port=$APPDYNAMICS_EUM_ANALYTICS_PORT" \
        $APPD_EUM_PROC_PROP
    sed -i -e "/analytics.accountAccessKey/c\analytics\.accountAccessKey=$APPDYNAMICS_EUM_ANALYTICS_KEY" \
        $APPD_EUM_PROC_PROP  
    echo "$(date -u +%d\ %b\ %Y\ %H:%M:%S) INFO [start.sh] \
        All EUM Analytics variables set. Will configure it."
else
    echo "$(date -u +%d\ %b\ %Y\ %H:%M:%S) INFO [start.sh] \
        Not all EUM Analytics variables set."
fi
rm -rf $APPD_EUM_PROC/pid.txt $APPD_EUM_DB/mysql.pid $APPD_EUM_DB/mysql.sock.lock 
$APPD_EUM_DB/bin/mysqld --defaults-file=$APPD_EUM_DB/db.cnf --daemonize
cd $APPD_EUM_PROC
./bin/eum.sh start
tail -f /dev/null
