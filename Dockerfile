FROM ubuntu AS builder
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

ARG BASEURL=https://download.appdynamics.com/download/prox/download-file
ARG EUM_VERSION
ARG SYNTHETICS_VERSION
ARG USER
ARG PASSWORD
ARG DB_HOST="localhost"
ARG DB_PORT="3388"
ARG DB_ROOT_USER="root"
ARG DB_ROOT_PWD="appdynamics"
ARG DB_USERNAME="eum_user"
ARG DB_USER_PWD="appdynamics"
ARG COLLECTOR_HOST="localhost"
ARG COLLECTOR_PORT="7001"
ARG KEY_STORE_PASSWORD="appdynamics"

ADD eum.response /tmp/
ADD start.sh /opt/appdynamics/eum/

RUN apt-get update
RUN apt-get install --fix-missing -q -y libaio1 libnuma1 net-tools curl tar unzip wget python python-pip
RUN echo "ulimit -n 65535" >> /etc/profile
RUN echo "ulimit -u 8192" >> /etc/profile
RUN echo "vm.swappiness = 10" >> /etc/sysctl.conf
RUN curl --referer http://www.appdynamics.com -c /tmp/cookies.txt -d "username=${USER}&password=${PASSWORD}" https://login.appdynamics.com/sso/login/
RUN curl -L -o /tmp/euem-64bit-linux.sh -b /tmp/cookies.txt ${BASEURL}/euem-processor/${EUM_VERSION}/euem-64bit-linux-${EUM_VERSION}.sh
RUN chmod +x /tmp/euem-64bit-linux.sh
RUN chmod +x /opt/appdynamics/eum/start.sh
RUN /tmp/euem-64bit-linux.sh -q -varfile /tmp/eum.response
RUN curl --referer http://www.appdynamics.com -c /tmp/cookies.txt -d "username=${USER}&password=${PASSWORD}" https://login.appdynamics.com/sso/login/
RUN curl -L -o /tmp/appdynamics-synthetic-server.zip -b /tmp/cookies.txt ${BASEURL}/synthetic-server/${SYNTHETICS_VERSION}/appdynamics-synthetic-server-${SYNTHETICS_VERSION}.zip
RUN mkdir -p /opt/appdynamics/synthetics
RUN unzip /tmp/appdynamics-synthetic-server.zip -d /opt/appdynamics/synthetics
RUN cd /opt/appdynamics/synthetics
RUN cp inputs.groovy.sample inputs.groovy
RUN sed -i -e "/db_host/c\db_host = \"${DB_HOST}\"" inputs.groovy
RUN sed -i -e "/db_port/c\db_port = \"${DB_PORT}\"" inputs.groovy
RUN sed -i -e "/db_root_user/c\db_root_user = \"${DB_ROOT_USER}\"" inputs.groovy
RUN sed -i -e "/db_root_pwd/c\db_root_pwd = \"${DB_ROOT_PWD}\"" inputs.groovy
RUN sed -i -e "/db_username/c\db_username = \"${DB_USERNAME}\"" inputs.groovy
RUN sed -i -e "/db_user_pwd/c\db_user_pwd = \"${DB_USER_PWD}\"" inputs.groovy
RUN sed -i -e "/collector_host/c\collector_host = \"${COLLECTOR_HOST}\"" inputs.groovy
RUN sed -i -e "/collector_port/c\collector_port = \"${COLLECTOR_PORT}\"" inputs.groovy
RUN sed -i -e "/key_store_password/c\key_store_password = \"${KEY_STORE_PASSWORD}\"" inputs.groovy
RUN sed -i -e "/JAVA_HOME/c\readonly JAVACMD=\"${JAVA_HOME}\/bin\/java\"" synthetic-processor/synthetic-sql-store/bin/synthetic-sql-schema.sh

ENV JAVA_HOME /opt/appdynamics/eum

RUN unix/deploy.sh install

FROM ubuntu
LABEL maintainer="Michael Englert <michi.eng@gmail.com>"

COPY --from=builder /opt/appdynamics /opt/appdynamics
COPY --from=builder /etc/profile /etc/profile
COPY --from=builder /etc/sysctl.conf /etc/sysctl.conf

ENV APPDYNAMICS_EUM_ANALYTICS_SCHEME="http" \
    APPDYNAMICS_EUM_ANALYTICS_HOST="localhost" \
    APPDYNAMICS_EUM_ANALYTICS_PORT="9080" \
    APPDYNAMICS_EUM_ANALYTICS_KEY=""

RUN apt-get update \
    && apt-get install --fix-missing -q -y libaio1 libnuma1 wget python python-pip

EXPOSE 7001

CMD [ "/bin/bash", "-c", "/opt/appdynamics/eum/start.sh" ]